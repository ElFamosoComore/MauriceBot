const Discord = require('discord.js');
const bot = new Discord.Client({autoReconnect:true});
const YTDL = require('ytdl-core')

function play(connection, message) {
    var server = servers[message.guild.id];

    server.dispatcher = connection.playStream(YTDL(server.queue[0], {filter: "audioonly"}));

    server.queue.shift();

    server.dispatcher.on("end", function() {
        if(server.queue[0]) {
            play(connection, message)
        } else connection.disconnect();
    })
}


bot.on('guildMemberAdd', function (member) {
    let nonVerifiedRole = member.guild.roles.find('name', 'Non-vérifié');
    member.addRole(nonVerifiedRole)
});

bot.on("ready", function() {
    console.log("Ready");

    bot.user.setActivity("regarder vos messages.")
});

bot.on('message', function (message) {
    if(message.content.startsWith("!clear")) {
        if(!message.member.roles.find('name', "Membre de l'administration")) {
            if(!message.member.roles.find('name', "Membre de la modération")) {
                message.member.send(":satellite: Vous n'avez pas la permission.")
                message.delete()
                return;
            }
        }


        message.channel.fetchMessages()
            .then(function (list) { message.channel.bulkDelete(list) }, function (err) { console.log(err) })

    }});


bot.on('message', function (message) {
    if(message.content.startsWith('!help')) {
        message.channel.send({embed: {

                color: 3447003,
                author: {
                    icon: "http://image.noelshack.com/fichiers/2018/28/3/1531313112-logo.jpg"
                },

                title: "Aide",
                description: "Commandes de Maurice le robot",
                fields: [{
                    name: "!play",
                    value: "Bientôt",
                 }, {
                    name: "!usermod",
                    value: "Met le grade \"Membre de la modération\" (Réservé aux \"Membre de l'administration\")",
                 }, {
                    name: "!useradmin",
                    value: "Met le grade \"Membre de l'administration\" (Réservé aux \"Gérants\")",
                 }, {
                    name: "!queue",
                    value: "Voir la queue du bot musical",
                 }, {
                    name: "!skip",
                    value: "Passer la musique entrain de passer par le bot musical",
                 }, {
                    name: "!stop",
                    value: "Arrêter la musique et efface la queue",
                 }, {
                    name: "!play <lien>",
                    value: "Lance ou ajoute à la queue la musique définie",
                 }, {
                    name: "!mute <@Personne#ID>",
                    value: "Rend muet la personne définie",
                 }],

                timestamp: new Date(),
                footer: {
                    text: "Demandé par " + message.member.displayName
                }

            }})
    }
});


bot.on('message', function (message) {
    if(message.content.startsWith('!usermod') && message.content != "!usermod") {
        let modRole = message.guild.roles.find('name', 'Membre de la modération');
        if(message.member.roles.find('name', "Membre de l'administration")) {
            if(message.mentions.members.first().roles.find('name', 'Membre de la modération')) {
                message.mentions.members.first().removeRole(modRole).catch(console.error);
                message.channel.send(':white_check_mark: Le grade "Membre de la modération" a été enlevé à ' + message.mentions.members.first().displayName + " !")
            } else {
                message.mentions.members.first().addRole(modRole).catch(console.error);
                message.channel.send(':white_check_mark: Le grade "Membre de la modération" a été ajouté à ' + message.mentions.members.first().displayName + " !")
            }
        } else {
            message.delete();
            message.member.send(":satellite: Vous n'avez pas la permission d'effectuer cette commande.")
        }
    } else if(message.content === "!usermod") {
        if(message.member.roles.find('name', "Membre de l'administration")) {
            message.channel.send("L'utilisation de cette commande est la suivante: !usermod <@Personne#ID>")
        } else {
            message.member.send(":satellite: Vous n'avez pas la permission d'effectuer cette commande.")
        }
    }
});

bot.on('message', function (message) {
    if(message.content.startsWith('!useradmin')) {
        if(message.content === "!useradmin") {
            if(message.member.roles.find('name', "Membre de l'administration")) {
                message.channel.send("L'utilisation de cette commande est la suivante: !useradmin <@Personne#ID>")
            } else {
                message.member.send(":satellite: Vous n'avez pas la permission d'effectuer cette commande.")
            }
        } else {
            let modRole = message.guild.roles.find('name', "Membre de l'administration");
            if(message.member.roles.find('name', "Gérant")) {
                if(message.mentions.members.first().roles.find('name', "Membre de l'administration")) {
                    message.mentions.members.first().removeRole(modRole).catch(console.error);
                    message.channel.send(':white_check_mark: Le grade "Membre de l\'administration" a été enlevé à ' + message.mentions.members.first().displayName + " !")
                } else {
                    message.mentions.members.first().addRole(modRole).catch(console.error);
                    message.channel.send(':white_check_mark: Le grade "Membre de l\'administration" a été ajouté à ' + message.mentions.members.first().displayName + " !")
                }
            } else {
                message.member.send(":satellite: Vous n'avez pas la permission d'effectuer cette commande.")
            }
        }
    }


});

bot.on('message', function (message) {
    if(message.content.startsWith('!mute')) {
        if(message.content === "!mute") {
            if(message.member.roles.find('name', "Membre de l'administration") || message.member.roles.find('name', "Membre de la modération")) {
                message.channel.send("L'utilisation de cette commande est la suivante: !mute <@Personne#ID>")
            } else {
                message.member.send(":satellite: Vous n'avez pas la permission d'effectuer cette commande.")
            }
        } else {
            let modRole = message.guild.roles.find('name', "Muet");
            if(message.member.roles.find('name', "Membre de la modération") || message.member.roles.find('name', "Membre de l'administration")) {
                if(message.mentions.members.first().roles.find('name', "Muet")) {
                    message.mentions.members.first().removeRole(modRole).catch(console.error);
                    message.channel.send(':white_check_mark: Le grade "Muet" a été enlevé à ' + message.mentions.members.first().displayName + " !")
                } else {
                    message.mentions.members.first().addRole(modRole).catch(console.error);
                    message.channel.send(':white_check_mark: Le grade "Muet" a été ajouté à ' + message.mentions.members.first().displayName + " !")
                }
            } else {
                message.member.send(":satellite: Vous n'avez pas la permission d'effectuer cette commande.")
            }
        }
    }


});

bot.on('message', function (message) {
    if(message.content.startsWith('!accept') && message.content != "!accept") {
        let modRole = message.guild.roles.find('name', "Non-vérifié");
        if(message.member.roles.find('name', "Membre de l'administration")) {
            if(!message.mentions.members.first().roles.find('name', "Non-vérifié")) {
                message.mentions.members.first().setRoles(null).catch(console.error);
                message.mentions.members.first().addRole(modRole);
                message.channel.send(':white_check_mark: Le grade "Non-vérifié" a été ajouté à ' + message.mentions.members.first().displayName + " !")
            } else {
                message.mentions.members.first().removeRole(modRole).catch(console.error);
                message.mentions.members.first().addRole(message.guild.roles.find('name', 'Potes'));
                message.channel.send(':white_check_mark: Le grade "Non-vérifié" a été enlevé à ' + message.mentions.members.first().displayName + " !")
            }
        } else {
            message.member.send(":satellite: Vous n'avez pas la permission d'effectuer cette commande.")
        }
    } else if(message.content === "!accept") {
        if(message.member.roles.find('name', "Membre de l'administration")) {
            message.channel.send("L'utilisation de cette commande est la suivante: !accept <@Personne#ID>")
        } else {
            message.member.send(":satellite: Vous n'avez pas la permission d'effectuer cette commande.")
        }
    }
});

bot.on('message', function (message) {

    if(message.content.includes('!ce')) {
        message.delete()
    }

});



var servers = {};

bot.on('message', function (message) {
    if(message.content.startsWith("!play")) {


        if(!message.member.roles.find('name', "Membre de l'administration")) {
            if(!message.member.roles.find('name', "Membre de la modération")) {
                message.member.send(":satellite: Vous n'avez pas la permission.")
                message.delete()
                return;
            }
        }

        if(message.content === "!play") {
            message.channel.send(':satellite: Utilisation correcte: !play <lien>')
            message.delete()
        } else {
            if(!message.member.voiceChannel) {
                message.channel.send("Vous devez être dans un canal vocal.")
                return;
            }

            if(!servers[message.guild.id]) servers[message.guild.id] = {
                queue: []
            };

            if(!message.content.replace('!play ', '').startsWith("https://www.youtube.com/watch?")) {
                message.channel.send(':satellite: Seuls les liens YouTube sont acceptés.')
                return;
            }

            var server = servers[message.guild.id];

            server.queue.push(message.content.replace('!play ', ''))
            message.channel.send(":satellite: Téléchargement...")
            message.channel.send(":satellite: Ajout à la liste...")

            if(!message.guild.voiceConnection) message.member.voiceChannel.join().then(function (connection) {

                play(connection, message)
                message.delete()
                message.channel.send(":satellite: Maurice se connecte...")
                message.channel.send(":satellite: Lien de musique/vidéo téléchargé: " + message.content.replace('!play ', ''))
            })


        }
    }
})

bot.on('message', function (message) {

    if(message.content.startsWith('!queue')) {

        if(!message.member.roles.find('name', "Membre de l'administration")) {
            if(!message.member.roles.find('name', "Membre de la modération")) {
                message.member.send(":satellite: Vous n'avez pas la permission.")
                message.delete()
                return;
            }
        }

        var server = servers[message.guild.id]
        message.channel.send("La queue est compsée de : ")
        server.queue.forEach(function (url) { message.channel.send("- " + url) })
        message.delete()
    }
})

bot.on('message', function (message) {
    if(message.content.startsWith('!skip')) {

        if(!message.member.roles.find('name', "Membre de l'administration")) {
            if(!message.member.roles.find('name', "Membre de la modération")) {
                message.member.send(":satellite: Vous n'avez pas la permission.")
                message.delete()
                return;
            }
        }

        var server = servers[message.guild.id]

        if(server.dispatcher) server.dispatcher.end()
    }
})

bot.on('message', function (message) {
    if(message.content.startsWith('!stop')) {

        if(!message.member.roles.find('name', "Membre de l'administration")) {
            if(!message.member.roles.find('name', "Membre de la modération")) {
                message.member.send(":satellite: Vous n'avez pas la permission.")
                message.delete()
                return;
            }
        }

        var server = servers[message.guild.id]

        if(message.guild.voiceConnection) message.guild.voiceConnection.disconnect();

        message.channel.send(":satellite: Maurice s'arrête...")
        message.delete()

        servers[message.guild.id] = null;
    }
})


bot.login('NDY2NTU4OTE5NTQ4ODYyNDc0.Did-wQ.GjXJEwAIPQQVbb2eEM7E6lxmcMk');